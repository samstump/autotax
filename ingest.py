import ctypes

class Taxonomy:

    def __init__(self, inputfile):
        def my_hash(x):
            return ctypes.c_size_t(hash(x)).value

        label_set = set()
        data = []
        with open(inputfile, encoding='utf-8') as file:
            data = list(map(lambda x: x.strip().split('|'), file))

        for item in data:
            label_set = label_set | set(item)

        # set of labels & ids
        labels = sorted(list(label_set))
        ids = list(map(lambda x: my_hash(x), labels))

        # two way map ids to labels
        self.label_to_id = dict(zip(labels, ids))
        self.id_to_label = dict(zip(ids, labels))

        self.encoded = []
        for d in data:
            self.encoded += [list(map(lambda x: self.label_to_id[x], d))]

        self.root = list(map(lambda t: t[0], filter(lambda x: len(x) == 1, self.encoded)))

    def id(self, string):
        return self.label_to_id[string]

    def label(self, id):
        return self.id_to_label[id]

    def ids(self, l):
        return list(map(lambda x: self.label_to_id[x], l))

    def labels(self, l):
        return list(map(lambda x: self.id_to_label[x], l))
